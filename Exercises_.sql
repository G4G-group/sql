----# Find the 5 oldest users ----

SELECT * FROM users
ORDER BY created_at LIMIT 5;

----# What day of the week do most users register on? ----

SELECT 
DAYNAME(created_at) AS day,
COUNT(*) AS total
FROM users
GROUP BY day
ORDER BY total DESC
LIMIT 2;


----# Find the users who have never posted a photo ----

SELECT username,
photos.created_at
FROM users
LEFT JOIN photos
ON users.id = photos.user_id
WHERE photos.created_at IS NULL
;

----# What are the top 5 most commonly used hashtags? ----


SELECT tag_name,
COUNT(*) AS total
FROM tags
INNER JOIN photo_tags
ON tags.id = photo_tags.tag_id
GROUP BY tag_name
ORDER BY total DESC
LIMIT 5;

----# find users who have liked every single photo on the site ----

SELECT 
username,
COUNT(*) AS num_likes
FROM users
INNER JOIN likes
ON users.id = likes.user_id
GROUP BY likes.user_id
HAVING num_likes = (SELECT Count(*) FROM photos)
;















